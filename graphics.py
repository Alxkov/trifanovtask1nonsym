'''Milena'''

import matplotlib.pyplot as plt
import numpy as np


def draw_track(tau_array, coords_array, velocity_array, l0, l, L, y, F_max):
    gate1_left = (l0 + l - y / 2, (1 - L) / 2)
    gate1_right = (l0 + l + y / 2, (1 - L) / 2)
    gate2_left = (l0 - y / 2, (1 + L) / 2)
    gate2_right = (l0 + y / 2, (1 + L) / 2)
    x_points = [i[0] for i in coords_array]
    y_points = [i[1] for i in coords_array]
    plt.plot(x_points, y_points, '.', markersize=3)
    plt.grid()
    plt.xticks(np.arange(min(x_points), max(x_points) + 1, 0.1))
    plt.yticks(np.arange(min(x_points), max(x_points) + 1, 0.05))
    cor = 0.01
    fence_ll = [(0, gate1_left[1]), (gate1_left[0] - cor, gate1_left[1])]
    fence_lr = [(gate1_right[0] + cor, gate1_right[1]), (1, gate1_right[1])]
    fence_tl = [(0, gate2_left[1]), (gate2_left[0] - cor, gate2_left[1])]
    fence_tr = [(gate2_right[0] + cor, gate2_right[1]), (1, gate2_right[1])]
    plt.plot([fence_ll[0][0], fence_ll[1][0]], [fence_ll[0][1], fence_ll[1][1]])
    plt.plot([fence_lr[0][0], fence_lr[1][0]], [fence_lr[0][1], fence_lr[1][1]])
    plt.plot([fence_tl[0][0], fence_tl[1][0]], [fence_tl[0][1], fence_tl[1][1]])
    plt.plot([fence_tr[0][0], fence_tr[1][0]], [fence_tr[0][1], fence_tr[1][1]])
    plt.show()
    print('track is drawn')
