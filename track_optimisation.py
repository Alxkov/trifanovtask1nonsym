'''Alena'''
import math
from random import random
from math import sqrt

m = 1
v_max = [10, 10]


def v_i(q_1, q_2):
    result = [(q_2[0] - q_1[0]) / delta_t(q_1, q_2), (q_2[1] - q_1[1]) / delta_t(q_1, q_2)]
    return result


def delta_t(q_1, q_2):
    return max(math.fabs(q_2[0] - q_1[0]) / v_max[0], math.fabs(q_2[1] - q_1[1]) / v_max[1])


def t_b_i(q_1, q_2, q_3):
    v_1 = v_i(q_1, q_2)
    v_2 = v_i(q_2, q_3)
    return max(math.fabs(v_2[0] - v_1[0])/a_max[0], math.fabs(v_2[1] - v_1[1])/a_max[1])


def find_opt_track_time(tau_array, coord_array, F_max):
    a_max = [F_max/m, F_max/m]
    len_xy = len(coord_array)
    result = max(v_i(coord_array[0], coord_array[1])[0], v_i(coord_array[0], coord_array[1])[1])/(2 * a_max[0]) + max(v_i(coord_array[len_xy - 2], coord_array[len_xy - 1])[0], v_i(coord_array[len_xy - 2], coord_array[len_xy - 1])[1])/(2 * a_max[0])
    for i in range(1, len(coord_array) - 1):
        result += delta_t(coord_array[i], coord_array[i+1])
    return result


def find_opt_track_time_random(tau_array, coord_array, F_max):
    return random()
