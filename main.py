import graphics
import track_optimisation
import track_optimisation_new
import track_gen
import graphics_new

if __name__ == '__main__':
    eps = 0.01
    n = 100
    L, l0, l, y, F_max = [float(inp) for inp in input().split()]
    tau_array = [i / n for i in range(n + 1)]
    coord_array = track_gen.track_generation(tau_array, L, l0, l, y, F_max)
    velocity_array = []
    track = track_optimisation_new.find_opt_track_time(tau_array, coord_array, F_max)
    time = track[0]
    print('Track time is ' + '%.4f' % time)
    graphics_new.multicolored_lines(tau_array, coord_array, track[1], l0, l, L, y, F_max, 'Velocity', track[0])
    graphics_new.multicolored_lines(tau_array, coord_array, track[2], l0, l, L, y, F_max, 'Acceleration', track[0])
