'''Alex'''

import track_optimisation
from enum import Enum
from math import ceil, factorial, sqrt, fabs, copysign
import track_optimisation_new

eps = 0.01


def get_distance(p1, p2):
    return sqrt((p2[0] - p1[0]) ** 2 + (p2[1] - p1[1]) ** 2)


def get_track_length(coord_arr):
    length = 0
    for seg_id, seg in enumerate(coord_arr[-1]):
        length += get_distance(coord_arr[seg_id], coord_arr[seg_id + 1])
    return length


class Direction(Enum):
    LEFT = 0
    RIGHT = 1


def gen_bezier_n_ord(tau_array, curve_ord, points):
    result = [(0, 0) for _ in tau_array]
    for (tau_num, tau) in enumerate(tau_array):
        for pwr in range(curve_ord + 1):
            result[tau_num] = (result[tau_num][0] + factorial(curve_ord) / factorial(pwr) / factorial(curve_ord - pwr) * points[pwr][0] * tau ** pwr * (1 - tau) ** (curve_ord - pwr), result[tau_num][1] + factorial(curve_ord) / factorial(pwr) / factorial(curve_ord - pwr) *points[pwr][1] * tau ** pwr * (1 - tau) ** (curve_ord - pwr))
    return result


# def check_gate2(gate_coords, direc, tau_array, coords_array):
#     """works only at the big number of len(coords_array)"""
#     global eps
#     if coords_array[0][0] > gate_coords[1]:
#         return True
#     if coords_array[len(coords_array) - 1][0] < gate_coords[1]:
#         return True
#     upside = - 1
#     for i, coords in enumerate(coords_array):
#         if (gate_coords[1] - coords_array[i][1]) * upside > 0:
#             upside = upside * -1
#             if direc == Direction.LEFT and (coords[0] + coords_array[i - 1][0]) / 2 < gate_coords[0] - eps:
#                 return False
#             elif direc == Direction.RIGHT and (coords[0] + coords_array[i - 1][0]) / 2 > gate_coords[0] + eps:
#                 return False
#     return True


def check_gate(gate_coords, direc, tau_array, coords_array):
    """works only at the big number of len(coords_array)"""
    global eps
    if coords_array[0][0] > gate_coords[1]:
        return True
    if coords_array[len(coords_array) - 1][0] < gate_coords[1]:
        return True
    for i, coords in enumerate(coords_array):
        if direc == Direction.LEFT and coords[0] < gate_coords[0] + eps and gate_coords[1] - eps < coords[1] < gate_coords[1] + eps:
            return False
        elif direc == Direction.RIGHT and coords[0] > gate_coords[0] - eps and gate_coords[1] - eps < coords[1] < gate_coords[1] + eps:
            return False
    return True


def cut_tau_array_in_interval(tau_array, left, right):
    length = len(tau_array)
    if length == 0:
        return []
    if length == 1:
        if left < tau_array[0] < right:
            return tau_array
        else:
            return []
    left_int = int(left * (length - 1))
    right_int = int(right * (length - 1))
    return tau_array[left_int: right_int + 1]


def scale_cut_to_full(cut_tau_array):
    if not cut_tau_array:
        return []
    if len(cut_tau_array) == 1:
        return cut_tau_array
    new_step = 1 / (len(cut_tau_array) - 1)
    return [i * new_step for i, _ in enumerate(cut_tau_array)]


def smart_join_coords(*coord_arrays):
    if not coord_arrays:
        return []
    res = coord_arrays[0]
    for arr in coord_arrays[1:]:
        if not arr:
            continue
        if res[-1] == arr[0]:
            res += arr[1:]
        else:
            res += arr
    return res


def generate_polyline(tau_array, L, l, y):
    tau_start = 0
    tau_gate1 = 2 * (1 + l) / 2 / (1 + 2 * l)
    tau_center = 1
    start = (0, 0)
    gate1 = ((1 + l) / 2, (1 - L) / 2)
    center = (1 / 2, 1 / 2)
    start_gate1 = gen_bezier_n_ord(scale_cut_to_full(cut_tau_array_in_interval(tau_array, tau_start, tau_gate1)),
                                   1, [start, gate1])
    gate1_center = gen_bezier_n_ord(scale_cut_to_full(cut_tau_array_in_interval(tau_array, tau_gate1, tau_center)),
                                   1, [gate1, center])
    return smart_join_coords(start_gate1, gate1_center)


def generate_polyline_custom_points(tau_array, start, turn, finish):
    start_turn_length = get_distance(start, turn)
    turn_finish_length = get_distance(turn, finish)
    tau_start = 0
    tau_turn = start_turn_length / (start_turn_length + turn_finish_length)
    tau_finish = 1
    start_turn = gen_bezier_n_ord(scale_cut_to_full(cut_tau_array_in_interval(tau_array, tau_start, tau_turn)),
                                   1, [start, turn])
    turn_center = gen_bezier_n_ord(scale_cut_to_full(cut_tau_array_in_interval(tau_array, tau_turn, tau_finish)),
                                   1, [turn, finish])
    return smart_join_coords(start_turn, turn_center)


def generate_polyline_custom_points_2(tau_array, start, turn1, turn2, finish):
    start_turn1_length = get_distance(finish, turn1)
    turn1_turn2_length = get_distance(turn1, turn2)
    turn2_finish_length = get_distance(turn2, finish)
    tau_start = 0
    tau_turn1 = start_turn1_length / (start_turn1_length + turn1_turn2_length + turn2_finish_length)
    tau_turn2 = (start_turn1_length + turn1_turn2_length) / (start_turn1_length + turn1_turn2_length
                                                             + turn2_finish_length)
    tau_finish = 1
    start_turn1 = gen_bezier_n_ord(scale_cut_to_full(cut_tau_array_in_interval(tau_array, tau_start, tau_turn1)),
                                   1, [start, turn1])
    turn1_turn2 = gen_bezier_n_ord(scale_cut_to_full(cut_tau_array_in_interval(tau_array, tau_turn1, tau_turn2)),
                                   1, [turn1, turn2])
    turn2_finish = gen_bezier_n_ord(scale_cut_to_full(cut_tau_array_in_interval(tau_array, tau_turn2, tau_finish)),
                                   1, [turn2, finish])
    return smart_join_coords(start_turn1, turn1_turn2, turn2_finish)


def gen_simple_2_ord_bezier_trajectory(tau_array, L, l, y):
    gate1_right = ((1 + l + y) / 2, (1 - L) / 2)
    return gen_bezier_n_ord(tau_array, 2, [(0, 0), gate1_right, (1 / 2, 1 / 2)])


def find_interim_tau_inds_gate1(tau_array, polyline, right_cutoff_x_coord):
    "Find the pair of taus for the most right points of polyline, if there are no any, exception is thrown"
    if len(polyline) < 6:
        raise AttributeError('Insufficient number of points in polyline')
    x_diff_lower = polyline[1][0] - polyline[0][0]
    x_diff_higher = polyline[-2][0] - polyline[-1][0]
    tau_ind1 = int((right_cutoff_x_coord - polyline[0][0]) / x_diff_lower)
    tau_ind2 = len(tau_array) - ceil((right_cutoff_x_coord - polyline[-1][0]) / x_diff_higher) - 1
    if polyline[tau_ind1][0] > right_cutoff_x_coord:
        raise AttributeError('polyline has improper vertex')
    return tau_ind1, tau_ind2


def find_interim_tau_inds_gate2(tau_array, polyline, right_cutoff_x_coord):
    "Find the pair of taus for the most right points of polyline, if there are no any, exception is thrown"
    if len(polyline) < 6:
        raise AttributeError('Insufficient number of points in polyline')
    x_diff_lower = polyline[1][0] - polyline[0][0]
    x_diff_higher = polyline[-2][0] - polyline[-1][0]
    tau_ind1 = int((right_cutoff_x_coord - polyline[0][0]) / x_diff_lower)
    tau_ind2 = len(tau_array) - ceil((right_cutoff_x_coord - polyline[-1][0]) / x_diff_higher) - 1
    if polyline[tau_ind1][0] > right_cutoff_x_coord:
        raise AttributeError('polyline has improper vertex')
    return tau_ind1, tau_ind2


def gen_3_ord_bezier_partial(tau_array, vertex, tau_first=0, tau_last=1/2):
    tau_step = tau_array[1] - tau_array[0]
    first_ind = ceil(tau_first / tau_step)
    last_ind = int(tau_last / tau_step)
    poly_aux = generate_polyline_custom_points(tau_array, (0, 0), (vertex[0], vertex[1]), (1 / 2, 1 / 2))
    linear_part1 = poly_aux[: first_ind + 1]
    linear_part2 = poly_aux[last_ind:]
    bez_point1 = linear_part1[-1]
    bez_point2 = linear_part2[0]
    try:
        interim_tau_inds = find_interim_tau_inds_gate1(tau_array, poly_aux, vertex[0])
        bezier_part = gen_bezier_n_ord(scale_cut_to_full(cut_tau_array_in_interval(tau_array, tau_array[first_ind],
                                                                                   tau_array[last_ind])), 3,
                                       [bez_point1, poly_aux[interim_tau_inds[0]],
                                        poly_aux[interim_tau_inds[1]], bez_point2])
    except AttributeError:
        bezier_part = []
    return smart_join_coords(linear_part1, bezier_part, linear_part2)


def gen_3_ord_bezier_general_partial(tau_array, start, vertex, finish, tau_first=0,
                                     tau_one=1/3, tau_two=2/3,   tau_last=1):
    tau_step = tau_array[1] - tau_array[0]
    first_ind = ceil(tau_first / tau_step)
    one_ind = ceil(tau_one / tau_step)
    two_ind = int(tau_two / tau_step)
    last_ind = int(tau_last / tau_step)
    poly_aux = generate_polyline_custom_points(tau_array, start, (vertex[0], vertex[1]), finish)
    linear_part1 = poly_aux[: first_ind + 1]
    linear_part2 = poly_aux[last_ind:]
    bez_point1 = linear_part1[-1]
    bez_point2 = linear_part2[0]
    point_one = poly_aux[one_ind]
    point_two = poly_aux[two_ind]
    bezier_part = gen_bezier_n_ord(scale_cut_to_full(cut_tau_array_in_interval(tau_array, tau_array[first_ind],
                                    tau_array[last_ind])), 3, [bez_point1, point_one,
                                        point_two, bez_point2])
    return smart_join_coords(linear_part1, bezier_part, linear_part2)


def reflect_center_symmetrically(tau_array, coords_array):
    res = coords_array.copy()
    return res[:-2:2] + [(1 - i[0], 1 - i[1]) for i in res[::2][::-1]]


# def try_3_ord_bezier2(tau_array, L, l0, l, y):
#     gate1_left = (l0 + l - y / 2, (1 - L) / 2)
#     gate1_right = (l0 + l + y / 2, (1 - L) / 2)
#     gate2_left = (l0 - y / 2, (1 + L) / 2)
#     gate2_right = (l0 + y / 2, (1 + L) / 2)
#     gate1 = ((gate1_left[0] + gate1_right[0]) / 2, (gate1_right[1] + gate1_left[1]) / 2)
#     gate2 = ((gate2_left[0] + gate2_right[0]) / 2, (gate2_right[1] + gate2_left[1]) / 2)
#     center = ((gate1[0] + gate2[0]) / 2, (gate1[1] + gate2[1]) / 2)
#     vertex1_x_array = [0.75 * gate1_left + i / 10 * (1.2 * gate1_right - 0.75 * gate1_left) for i in range(10)]
#     vertex2_x_array = [2 * center[0] - i for i in vertex1_x_array]
#     best_solution = ()
#     for vertex1_x in vertex1_x_array:
#         vertex2_x = 2 * center[0] - vertex1_x
#         poly_aux = generate_polyline_custom_points(tau_array, (0, 0), (vertex1_x, gate1_right[1]), (vertex2_x, gate2_right[1]), (1, 1))
#         start = (0, 0)
#         finish = (1, 1)
#         turn1 = (vertex1_x, gate1_right[1])
#         turn2 = (vertex2_x, gate2_right[1])
#         start_turn1_length = get_distance(finish, turn1)
#         turn1_turn2_length = get_distance(turn1, turn2)
#         turn2_finish_length = get_distance(turn2, finish)
#         tau_start = 0
#         tau_turn1 = start_turn1_length / (start_turn1_length + turn1_turn2_length + turn2_finish_length)
#         tau_turn2 = (start_turn1_length + turn1_turn2_length) / (start_turn1_length + turn1_turn2_length
#                                                                  + turn2_finish_length)
#         tau_center = (tau_turn2 + tau_turn1) / 2
#         tau_finish = 1
#         tau_start_array = [(tau_turn1 - tau_start) * i / 10 for i in range(10)]
#         tau_end_array = [(tau_center - tau_turn1) * i / 10 for i in range(10)]
#         for tau_start in tau_start_array:
#             for tau_end in tau_end_array:
#                 tau_start_2 = tau_turn2 - (tau_end - tau_turn1)
#                 tau_end_2 = tau_finish - tau_start
#                 if tau_start > tau_end:
#                     raise ValueError("tau_start exceeds tau_end")
#                 coords_array_part1 = gen_3_ord_bezier_partial(tau_array, (vertex1_x, gate1_right[1]), tau_start,
#                                                              tau_end)
#                 coords_array_part2 = gen_3_ord_bezier_partial(tau_array, (vertex2_x, gate2_right[1]), tau_start_2,
#                                                               tau_end_2)
#                 coords_array = smart_join_coords(coords_array_part1, coords_array_part2)
#                 time = track_optimisation.find_opt_track_time(tau_array, coords_array, 10)
#                 if not check_gate(gate1_left, Direction.LEFT, tau_array, coords_array):
#                     continue
#                 if not best_solution:
#                     best_solution = (time, coords_array)
#                     continue
#                 if time < best_solution[0]:
#                     best_solution = (time, coords_array)
#     return best_solution[1]


def build_2_curve_symmetricaly(tau_array, L, l0, l, y, vertex_1, i, n_taus_s, j, n_taus_one):
    gate1_left = (l0 + l - y / 2, (1 - L) / 2)
    gate1_right = (l0 + l + y / 2, (1 - L) / 2)
    gate2_left = (l0 - y / 2, (1 + L) / 2)
    gate2_right = (l0 + y / 2, (1 + L) / 2)
    gate1 = ((gate1_left[0] + gate1_right[0]) / 2, (gate1_right[1] + gate1_left[1]) / 2)
    gate2 = ((gate2_left[0] + gate2_right[0]) / 2, (gate2_right[1] + gate2_left[1]) / 2)
    center = ((gate1[0] + gate2[0]) / 2, (gate1[1] + gate2[1]) / 2)
    # vertex1_x = 0.8
    vertex_2 = (2 * center[0] - vertex_1[0], 2 * center[1] - vertex_1[1])
    start = (0, 0)
    finish = (1, 1)
    aux_poly_length = get_distance(start, vertex_1) + get_distance(vertex_1, vertex_2) + get_distance(vertex_2, finish)
    # tau_center is tau of general_curve
    tau_center = (get_distance(start, vertex_1) + get_distance(vertex_1, center)) / aux_poly_length
    # print(gate1_left, gate1_right, gate2_left, gate2_right)
    # following taus are for half-curves
    # n_taus_s = 15
    tau_vert1 = get_distance(start, vertex_1) / (get_distance(start, vertex_1) + get_distance(vertex_1, center))
    tau_begin1 = i / n_taus_s * tau_vert1
    # n_taus_one = 8
    tau_one1 = tau_begin1 + j / n_taus_one * (tau_vert1 - tau_begin1)
    tau_finish = 1
    tau_two1 = tau_finish - tau_one1 / tau_vert1 + tau_one1
    tau_end1 = tau_finish - tau_begin1 / tau_vert1 + tau_begin1
    coords_array_part1 = gen_3_ord_bezier_general_partial(
        scale_cut_to_full(cut_tau_array_in_interval(tau_array, tau_array[0], tau_center)), start, vertex_1, center,
        tau_begin1, tau_one1, tau_two1, tau_end1)
    tau_vert2 = get_distance(center, vertex_2) / (get_distance(vertex_2, center)
                                                  + get_distance(vertex_2, finish))
    try:
        tau_begin2 = (1 - tau_end1) / (1 - tau_vert1) * tau_vert2
    except ZeroDivisionError:
        print('hello')
    tau_one2 = (1 - tau_two1) / (1 - tau_vert1) * tau_vert2
    tau_two2 = 1 - tau_one1 / tau_vert1 * (1 - tau_vert2)
    tau_end2 = 1 - tau_begin1 / tau_vert1 * (1 - tau_vert2)
    coords_array_part2 = gen_3_ord_bezier_general_partial(
        scale_cut_to_full(cut_tau_array_in_interval(tau_array, tau_center, tau_array[-1])), center, vertex_2, finish,
        tau_begin2, tau_one2, tau_two2, tau_end2)
    coords_array = smart_join_coords(coords_array_part1, coords_array_part2)
    return coords_array


def selection_of_time_optimal_trajectory(tau_array, L, l0, l, y, F_max):
    gate1_left = (l0 + l - y / 2, (1 - L) / 2)
    gate1_right = (l0 + l + y / 2, (1 - L) / 2)
    gate2_left = (l0 - y / 2, (1 + L) / 2)
    gate2_right = (l0 + y / 2, (1 + L) / 2)
    gate1 = ((gate1_left[0] + gate1_right[0]) / 2, (gate1_right[1] + gate1_left[1]) / 2)
    gate2 = ((gate2_left[0] + gate2_right[0]) / 2, (gate2_right[1] + gate2_left[1]) / 2)
    center = ((gate1[0] + gate2[0]) / 2, (gate1[1] + gate2[1]) / 2)
    best_solution = ()
    g = lambda x: fabs(x) ** 1.5
    vertex_x_array = [gate1[0] + y * copysign(1, i) * g(0.24 * i) for i in range(-6, 7)]
    n_taus_one = 8
    n_taus_s = 15
    n_vertex_y = 5
    vertex_y_array = [gate1[1] * (1 + 1 / 4 * i) for i in range(-2, -2 + n_vertex_y)]
    for vertex_y in vertex_y_array:
        for vertex_x in vertex_x_array:
            for tau_s in range(1, n_taus_s + 1):
                for tau_one in range(1, n_taus_one + 1):
                    if vertex_x == center[0] and vertex_y == center[1]:
                        continue
                    coord_array = build_2_curve_symmetricaly(tau_array, L, l0, l, y, (vertex_x, vertex_y), tau_s, n_taus_s, tau_one,
                                                             n_taus_one)
                    # time = track_optimisation.find_opt_track_time(tau_array, coord_array, F_max)
                    time = track_optimisation_new.find_opt_track_time(tau_array, coord_array, F_max)[0]
                    if not check_gate(gate1_left, Direction.LEFT, tau_array, coord_array):
                        continue
                    if not check_gate(gate1_right, Direction.RIGHT, tau_array, coord_array):
                        continue
                    if not check_gate(gate2_left, Direction.LEFT, tau_array, coord_array):
                        continue
                    if not check_gate(gate2_right, Direction.RIGHT, tau_array, coord_array):
                        continue
                    if not best_solution or time < best_solution[0]:
                        best_solution = (time, coord_array, (vertex_x, vertex_y, tau_s, tau_one))
    return best_solution


def track_generation(tau_array, L, l0, l, y, F_max):
    gate1_left = (l0 + l - y / 2, (1 - L) / 2)
    gate1_right = (l0 + l + y / 2, (1 - L) / 2)
    gate2_left = (l0 - y / 2, (1 + L) / 2)
    gate2_right = (l0 + y / 2, (1 + L) / 2)
    gate1 = ((gate1_left[0] + gate1_right[0]) / 2, (gate1_right[1] + gate1_left[1]) / 2)
    gate2 = ((gate2_left[0] + gate2_right[0]) / 2, (gate2_right[1] + gate2_left[1]) / 2)
    center = ((gate1[0] + gate2[0]) / 2, (gate1[1] + gate2[1]) / 2)
    g = lambda x: x ** 1.5
    vertex_x_array = [copysign(1, i) * g(fabs(0.24 * i)) for i in range(-6, 7)]
    # coords_array = build_2_curve_symmetricaly(tau_array, L, l0, l, y, (0.46, 0.20), 3, 15, 7, 8)
    # coords_array = build_2_curve_symmetricaly(tau_array, L, l0, l, y, (0.11854, 0.125), 8, 15, 5, 8)
    coords_array = selection_of_time_optimal_trajectory(tau_array, L, l0, l, y, F_max)[1]
    # print(coords_array)
    print(check_gate(gate1_left, Direction.LEFT, tau_array, coords_array))
    print(check_gate(gate1_right, Direction.RIGHT, tau_array, coords_array))
    print("track length is " + str(get_track_length(coords_array)))
    return coords_array
# 0.046504083237770284
# 0.052030255953033615
