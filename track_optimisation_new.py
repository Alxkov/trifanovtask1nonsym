'''Alex again'''

import math
from random import random
from math import sqrt, fabs
from track_gen import get_distance, Direction

min_curvature = 10 ** 8
m = 1


def get_point_curve_rad(p1, p2, p3):
    p1p2 = get_distance(p1, p2)
    p1p3 = get_distance(p1, p3)
    p2p3 = get_distance(p2, p3)
    try:
        if fabs((p1p2 ** 2 + p2p3 ** 2 - p1p3 ** 2) / (2 * p1p2 * p2p3)) > 0.997:
            return min_curvature
        return p1p2 / (2 * (1 / (sqrt(1 + ((p2p3 / p1p2 - (p1p2 ** 2 + p2p3 ** 2 - p1p3 ** 2) / (2 * p1p2 * p2p3)) /
                                    sqrt(1 - ((p1p2 ** 2 + p2p3 ** 2 - p1p3 ** 2) / (2 * p1p2 * p2p3)) ** 2)) ** 2))))
    except ZeroDivisionError:
        return min_curvature
    except ValueError:
        return min_curvature


# def get_radia_for_curve(coord_array):
#     return [min_curvature] + [get_point_curve_rad(coord_array[i_coord - 1], coord, coord_array[i_coord + 1])
#                               for i_coord, coord in enumerate(coord_array[1:-1])] + [min_curvature]


def get_radia_for_curve(coord_array):
    return [min_curvature] + [get_point_curve_rad(coord_array[i_coord - 1], coord_array[i_coord], coord_array[i_coord + 1])
                              for i_coord in range(1, len(coord_array) - 2)] + [min_curvature]


def get_max_speed_array(radia_array, F_max):
    a_max = F_max / m
    return [sqrt(a_max * radius) for radius in radia_array]


def find_next_function_minimum_value_ind(nods_array, function_values_array, start_nod_ind, direc: Direction):
    if type(direc) is not Direction:
        raise TypeError('direc must be of class Direction')
    function_cut = []
    if direc is Direction.RIGHT:
        function_cut = function_values_array[start_nod_ind:]
    elif direc is Direction.LEFT:
        function_cut = function_values_array[start_nod_ind::-1]
    for func_val_ind, func_val in enumerate(function_cut[:-1]):
        if func_val < function_cut[func_val_ind + 1]:
            if direc is Direction.RIGHT:
                return func_val_ind + start_nod_ind
            elif direc is Direction.LEFT:
                return start_nod_ind - func_val_ind
    return len(function_values_array) - 1


def back_from_min_to_current(coord_array, max_speed_square_array, vel_array, tan_arr_array, radia, start_ind, min_ind, a_max):
    vel_array[min_ind] = max_speed_square_array[min_ind]
    for ind in range(min_ind, start_ind, -1):
        delta_q = get_distance(coord_array[ind], coord_array[ind - 1])
        tan_arr_array[ind - 1] = - sqrt(a_max ** 2 - max_speed_square_array[ind - 1] ** 2 / radia[ind - 1])
        try:
            vel_array[ind - 1] = (vel_array[ind] + sqrt(vel_array[ind] ** 2 - 4 * tan_arr_array[ind - 1] * delta_q)) / 2
        except TypeError:
            print('hello')
    ind = start_ind
    delta_q = get_distance(coord_array[ind], coord_array[ind - 1])
    vel_diff = fabs(vel_array[ind] - vel_array[ind - 1])
    max_acc_ind = sqrt(a_max ** 2 - max_speed_square_array[ind - 1] ** 2 / radia[ind - 1])
    t_im1 = delta_q / vel_array[ind]
    while max_acc_ind * t_im1 < vel_diff and vel_array[ind - 1] > vel_array[ind]:
        vel_array[ind - 1] = vel_array[ind] + max_acc_ind * t_im1
        tan_arr_array[ind - 1] = -max_acc_ind
        ind -= 1
        try:
            delta_q = get_distance(coord_array[ind], coord_array[ind - 1])
        except IndexError:
            print('hello')
        vel_diff = fabs(vel_array[ind] - vel_array[ind - 1])
        max_acc_ind = sqrt(a_max ** 2 - max_speed_square_array[ind - 1] ** 2 / radia[ind - 1])
        t_im1 = delta_q / vel_array[ind]
    tan_arr_array[ind - 1] = vel_diff / t_im1


def find_opt_track_time(tau_array, coord_array, F_max):
    a_max = F_max / m
    radia = get_radia_for_curve(coord_array)
    max_speed_array = get_max_speed_array(radia, F_max)
    max_speed_array[-1] = 0
    vel_array = [0 for _ in coord_array]
    tan_arr_array = [0 for _ in coord_array] # tan
    current_point_ind = 0
    a_tan_max_i = max_speed_array[current_point_ind] ** 2 / radia[current_point_ind]
    delta_q = get_distance(coord_array[current_point_ind + 1], coord_array[current_point_ind])
    vel_array[current_point_ind] = sqrt(a_tan_max_i * delta_q / 2)
    while current_point_ind < len(coord_array) - 2:
        go_back = False
        try:
             a_tan_max_i = sqrt(a_max ** 2 - vel_array[current_point_ind] ** 4 / radia[current_point_ind] * 2)
        except ValueError:
            min_ind = find_next_function_minimum_value_ind(coord_array, max_speed_array,
                                                           current_point_ind, Direction.RIGHT)
            back_from_min_to_current(coord_array, max_speed_array, vel_array, tan_arr_array,
                                     radia, current_point_ind, min_ind, a_max)
            current_point_ind += 1
            continue
        delta_q = get_distance(coord_array[current_point_ind + 1], coord_array[current_point_ind])
        t_i = delta_q / vel_array[current_point_ind]
        if (a_tan_max_i * t_i + vel_array[current_point_ind]) < max_speed_array[current_point_ind + 1]:
            vel_array[current_point_ind + 1] = a_tan_max_i * t_i + vel_array[current_point_ind]
            tan_arr_array[current_point_ind] = a_tan_max_i
        elif (-a_tan_max_i * t_i + vel_array[current_point_ind]) < max_speed_array[current_point_ind + 1]:
            vel_array[current_point_ind + 1] = max_speed_array[current_point_ind + 1]
            tan_arr_array[current_point_ind] = (max_speed_array[current_point_ind + 1]
                                                - vel_array[current_point_ind]) / t_i
        else:
            min_ind = find_next_function_minimum_value_ind(coord_array, max_speed_array,
                                                           current_point_ind, Direction.RIGHT)
            back_from_min_to_current(coord_array, max_speed_array, vel_array, tan_arr_array,
                                     radia, current_point_ind, min_ind, a_max)
        current_point_ind += 1
    time = 0
    for ind in range(1, len(vel_array) - 2):
        time += get_distance(coord_array[ind], coord_array[ind + 1]) / vel_array[ind]
    return time, vel_array, tan_arr_array, radia
