'''Alex'''

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.collections as mcoll


def multicolored_lines(tau_array, cooord_array, vel_array, l0, l, L, y, F_max, title='Velocity', time=10):
    gate1_left = (l0 + l - y / 2, (1 - L) / 2)
    gate1_right = (l0 + l + y / 2, (1 - L) / 2)
    gate2_left = (l0 - y / 2, (1 + L) / 2)
    gate2_right = (l0 + y / 2, (1 + L) / 2)
    cor = 0.01
    fence_ll = [(0, gate1_left[1]), (gate1_left[0] - cor, gate1_left[1])]
    fence_lr = [(gate1_right[0] + cor, gate1_right[1]), (1, gate1_right[1])]
    fence_tl = [(0, gate2_left[1]), (gate2_left[0] - cor, gate2_left[1])]
    fence_tr = [(gate2_right[0] + cor, gate2_right[1]), (1, gate2_right[1])]

    """
    http://nbviewer.ipython.org/github/dpsanders/matplotlib-examples/blob/master/colorline.ipynb
    http://matplotlib.org/examples/pylab_examples/multicolored_line.html
    """

    x = np.array([p[0] for p in cooord_array])
    y = np.array([p[1] for p in cooord_array])
    fig, ax = plt.subplots()
    lc = colorline(x, y, vel_array, cmap='jet', norm=plt.Normalize(min(vel_array), max(vel_array)))
    ax.text(0.35, -0.11, 'Overall time is ' + "%.2f" % time)
    ax.plot([fence_ll[0][0], fence_ll[1][0]], [fence_ll[0][1], fence_ll[1][1]], color='k')
    ax.plot([fence_lr[0][0], fence_lr[1][0]], [fence_lr[0][1], fence_lr[1][1]], color='k')
    ax.plot([fence_tl[0][0], fence_tl[1][0]], [fence_tl[0][1], fence_tl[1][1]], color='k')
    ax.plot([fence_tr[0][0], fence_tr[1][0]], [fence_tr[0][1], fence_tr[1][1]], color='k')
    plt.colorbar(lc)
    plt.xlim(0.0, 1.0)
    plt.ylim(0.0, 1.0)
    plt.title(title)
    plt.show()


def colorline(
        x, y, z=None, cmap='copper', norm=plt.Normalize(0.0, 1.0),
        linewidth=1, alpha=1.0):
    """
    http://nbviewer.ipython.org/github/dpsanders/matplotlib-examples/blob/master/colorline.ipynb
    http://matplotlib.org/examples/pylab_examples/multicolored_line.html
    Plot a colored line with coordinates x and y
    Optionally specify colors in the array z
    Optionally specify a colormap, a norm function and a line width
    """

    # Default colors equally spaced on [0,1]:
    if z is None:
        z = np.linspace(0.0, 1.0, len(x))

    # Special case if a single number:
    # to check for numerical input -- this is a hack
    if not hasattr(z, "__iter__"):
        z = np.array([z])

    z = np.asarray(z)

    segments = make_segments(x, y)
    lc = mcoll.LineCollection(segments, array=z, cmap=cmap, norm=norm,
                              linewidth=linewidth, alpha=alpha)

    ax = plt.gca()
    ax.add_collection(lc)

    return lc

def make_segments(x, y):
    """
    Create list of line segments from x and y coordinates, in the correct format
    for LineCollection: an array of the form numlines x (points per line) x 2 (x
    and y) array
    """

    points = np.array([x, y]).T.reshape(-1, 1, 2)
    segments = np.concatenate([points[:-1], points[1:]], axis=1)
    return segments
